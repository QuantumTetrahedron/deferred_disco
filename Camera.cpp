
#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

Camera::Camera(glm::vec3 _position, glm::vec3 _up, float _yaw, float _pitch)
: position(_position),
  front(glm::vec3(0.0f,0.0f,-1.0f)),
  worldUp(_up),
  yaw(_yaw), pitch(_pitch),
  movementSpeed(3.0f),
  mouseSensitivity(0.25f) {
    updateVectors();
}

glm::mat4 Camera::GetViewMatrix() const {
    return glm::lookAt(position, position+front, up);
}

void Camera::ProcessKeyboard(Camera::Movement direction, float dt) {
    float velocity = movementSpeed * dt;
    glm::vec3 Front = glm::cross(worldUp, right);
    if(direction == Movement::FORWARD){
        position += Front * velocity;
    } else if(direction == Movement::BACKWARD){
        position -= Front * velocity;
    } else if(direction == Movement::LEFT){
        position -= right * velocity;
    } else if(direction == Movement::RIGHT){
        position += right * velocity;
    }
    if(position.x > max_x) position.x = max_x;
    if(position.x < min_x) position.x = min_x;
    if(position.z > max_z) position.z = max_z;
    if(position.z < min_z) position.z = min_z;
}

void Camera::ProcessMouseMovement(float xOffset, float yOffset) {
    xOffset *= mouseSensitivity;
    yOffset *= mouseSensitivity;
    yaw += xOffset;
    pitch += yOffset;

    if(pitch > 89.0f){
        pitch = 89.0f;
    } else if(pitch < -89.0f){
        pitch = -89.0f;
    }
    updateVectors();
}

void Camera::updateVectors() {
    glm::vec3 f;
    f.x = glm::cos(glm::radians(yaw)) * glm::cos(glm::radians(pitch));
    f.y = glm::sin(glm::radians(pitch));
    f.z = glm::sin(glm::radians(yaw)) * glm::cos(glm::radians(pitch));
    front = glm::normalize(f);
    right = glm::normalize(glm::cross(front, worldUp));
    up = glm::normalize(glm::cross(right, front));
}

void Camera::SetConstraints(float minx, float maxx, float minz, float maxz) {
    min_x = minx;
    max_x = maxx;
    min_z = minz;
    max_z = maxz;
}
