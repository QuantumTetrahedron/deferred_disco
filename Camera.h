
#ifndef DEFERRED_DISCO_CAMERA_H
#define DEFERRED_DISCO_CAMERA_H

#include <vector>
#include <glm/glm.hpp>

class Camera {
public:
    enum class Movement {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT
    };

    glm::vec3 position, front, up, right, worldUp;
    float yaw, pitch;
    float movementSpeed, mouseSensitivity;

    Camera(glm::vec3 _position = glm::vec3(0.0f), glm::vec3 _up = glm::vec3(0.0f,1.0f,0.0f), float _yaw = -90.0f, float pitch = 0.0f);

    void SetConstraints(float minx, float maxx, float minz, float maxz);

    glm::mat4 GetViewMatrix() const;
    void ProcessKeyboard(Movement direction, float dt);
    void ProcessMouseMovement(float xOffset, float yOffset);

private:
    void updateVectors();
    float min_x, max_x, min_z, max_z;
};


#endif //DEFERRED_DISCO_CAMERA_H
