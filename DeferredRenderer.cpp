
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <algorithm>
#include "DeferredRenderer.h"
#include "ResourceManager.h"
#include "ShadowMapper.h"

void DeferredRenderer::Init(int width, int height) {

    glGenFramebuffers(1, &gBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

    glGenTextures(1, &gPosition);
    glBindTexture(GL_TEXTURE_2D, gPosition);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);

    glGenTextures(1, &gNormal);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);

    glGenTextures(1, &gAlbedoSpec);
    glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedoSpec, 0);

    unsigned int attachments[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, attachments);

    unsigned int rboDepth;
    glGenRenderbuffers(1, &rboDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenFramebuffers(2, pingpongFBO);
    glGenTextures(2, pingpongBuffer);
    for (unsigned int i = 0; i < 2; i++)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[i]);
        glBindTexture(GL_TEXTURE_2D, pingpongBuffer[i]);
        glTexImage2D(
                GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL
        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(
                GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pingpongBuffer[i], 0
        );
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    lightingPassShader = ResourceManager::GetShader("lightingPass");
    lightingPassShader->Use();
    lightingPassShader->SetInt("gPosition", 0);
    lightingPassShader->SetInt("gNormal", 1);
    lightingPassShader->SetInt("gAlbedoSpec", 2);
    lightingPassShader->SetInt("prevOut", 3);

    ping = true;
    toneMapShader = ResourceManager::GetShader("toneMapping");
    toneMapShader->Use();
    toneMapShader->SetInt("lightingResult", 0);
    toneMapShader->SetInt("ambientBase", 1);

    for(int i = 0; i < BATCH_SIZE; i++) {
        shadowMaps[i] = ShadowMapper::CreateNewShadowMap();
    }
}

void DeferredRenderer::Render(const Camera& c, const Scene& scene) {

    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for(auto& o : scene.walls){
        o->Draw(c);
    }

    for(auto& model : ResourceManager::GetModels()){
        std::vector<std::shared_ptr<Object>> filtered;
        std::shared_ptr<Shader> shader;
        for(auto& o : scene.objects){
            if(o->model == model.second){
                filtered.push_back(o);
            }
        }
        std::vector<glm::mat4> transforms;
        std::transform(std::begin(filtered), std::end(filtered), std::back_inserter(transforms), [](std::shared_ptr<Object>& obj){return obj->GetTransform().GetModelMatrix();});
        std::vector<glm::vec3> colors;
        std::transform(std::begin(filtered), std::end(filtered), std::back_inserter(colors), [](std::shared_ptr<Object>& obj){return obj->colorTint;});

        if(!filtered.empty()){
            shader = filtered[0]->shader;
            shader->Use();
            shader->SetMat4("view", c.GetViewMatrix());
            model.second->DrawInstanced(*shader, transforms, colors);
        }
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[0]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[1]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    lightingPassShader->Use();
    lightingPassShader->SetVec3("viewPos", c.position);

    for(int i = 0; i < scene.lights.size(); i+=BATCH_SIZE){
        for(int j = 0; j < BATCH_SIZE && i+j < scene.lights.size(); j++){
            lightingPassShader->Use();
            scene.lights[i+j]->SetUniforms(*lightingPassShader, j);
            glm::mat4 matrix = scene.lights[i+j]->GetLightSpaceMatrix();
            ShadowMapper::FillShadowMap(shadowMaps[j], matrix, scene);
            shadowMaps[j].UpdateShader(*lightingPassShader, "lights["+std::to_string(j)+"]", j);
        }
        lightingPassShader->Use();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, gPosition);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, gNormal);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, pingpongBuffer[ping ? 1 : 0]);
        glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[ping ? 0 : 1]);
        renderQuad();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        ping = !ping;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    toneMapShader->Use();
    toneMapShader->SetFloat("exposure", 0.2f);
    glClearColor(0.05f, 0.05f, 0.3f, 1.0f);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pingpongBuffer[ping ? 1 : 0]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
    renderQuad();

}

void DeferredRenderer::renderQuad() {
    unsigned int quadVBO;
    if (quadVAO == 0)
    {
        float quadVertices[] = {
                // positions        // texture Coords
                -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
                -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
                1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
                1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);

}
