
#ifndef DEFERRED_DISCO_DEFERREDRENDERER_H
#define DEFERRED_DISCO_DEFERREDRENDERER_H

#include <memory>
#include "Drawable.h"
#include "ShadowMapper.h"
#include "Scene.h"

const int BATCH_SIZE = 16;

class DeferredRenderer {
public:
    void Init(int width, int height);
    void Render(const Camera& c, const Scene& scene);
private:
    unsigned int gBuffer;
    unsigned int gPosition, gNormal, gAlbedoSpec;

    unsigned int pingpongFBO[2];
    unsigned int pingpongBuffer[2];
    bool ping;

    void renderQuad();
    unsigned int quadVAO = 0;

    std::shared_ptr<Shader> lightingPassShader;
    std::shared_ptr<Shader> toneMapShader;

    ShadowMapper::ShadowMap shadowMaps[BATCH_SIZE];
};

#endif //DEFERRED_DISCO_DEFERREDRENDERER_H
