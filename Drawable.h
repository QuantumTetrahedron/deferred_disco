
#ifndef DEFERRED_DISCO_DRAWABLE_H
#define DEFERRED_DISCO_DRAWABLE_H

#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Transform.h"

class Drawable {
public:
    virtual void Draw(const Camera& camera) const = 0;
    std::shared_ptr<Model> model;
    std::shared_ptr<Shader> shader;
    [[nodiscard]] virtual Transform GetTransform() const = 0;
};

#endif //DEFERRED_DISCO_DRAWABLE_H
