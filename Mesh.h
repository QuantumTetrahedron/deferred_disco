
#ifndef DEFERRED_DISCO_MESH_H
#define DEFERRED_DISCO_MESH_H

#include <assimp/Importer.hpp>
#include <glm/glm.hpp>
#include <vector>
#include "Shader.h"

class Mesh {
public:
    struct Vertex{
        glm::vec3 Position = glm::vec3(0.0f);
        glm::vec2 TexCoords = glm::vec2(0.0f);
        glm::vec3 Normal = glm::vec3(0.0f);
        glm::vec3 Tangent = glm::vec3(0.0f);
        glm::vec3 Bitangent = glm::vec3(0.0f);
    };

    struct Tex{
        unsigned int id;
        std::string type;
        aiString path;
    };

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Tex> textures);
    void Draw(const Shader& shader) const;
    void DrawInstanced(const Shader& shader, const std::vector<glm::mat4>& instances) const;
private:
    friend class Model;
    unsigned int VAO, VBO, EBO;

    void SetupDraw(const Shader& shader) const;
    void SetupMesh();

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Tex> textures;
};

#endif //DEFERRED_DISCO_MESH_H
