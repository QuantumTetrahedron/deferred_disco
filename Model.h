
#ifndef DEFERRED_DISCO_MODEL_H
#define DEFERRED_DISCO_MODEL_H

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>
#include "Mesh.h"

class Model {
public:
    explicit Model(const std::string& filePath);
    void Draw(const Shader& shader) const;
    void DrawInstanced(const Shader& shader, const std::vector<glm::mat4>& instances, const std::vector<glm::vec3>& colors);
private:
    void Setup();
    void LoadModel(const std::string& filePath);
    void ProcessNode(aiNode* node, const aiScene* scene);
    Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);

    std::vector<Mesh::Tex> LoadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName, bool gammaCorrection);
    unsigned int TextureFromFile(const char* path, const std::string& directory, bool gammaCorrection);

    std::vector<Mesh::Tex> textures;
    std::vector<Mesh> meshes;
    std::string directory;
    unsigned int instanceBuffer, colorBuffer;
    unsigned int current_count = 0;
};

#endif //DEFERRED_DISCO_MODEL_H
