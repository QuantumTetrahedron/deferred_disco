
#include "Object.h"
#include "ResourceManager.h"

void Object::Draw(const Camera& camera) const {
    shader->Use();
    shader->SetMat4("view", camera.GetViewMatrix());
    shader->SetMat4("model", transform.GetModelMatrix());
    shader->SetVec3("colorTint", colorTint);
    model->Draw(*shader);
}

Object::Object(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, const std::string& m, const std::string& sh, bool castsShadows)
: transform(position, rotation, scale), castsShadows(castsShadows){
    model = ResourceManager::GetModel(m);
    shader = ResourceManager::GetShader(sh);
}

Transform Object::GetTransform() const {
    return transform;
}

std::shared_ptr<Model> Object::GetModel() const {
    return model;
}
