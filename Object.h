
#ifndef DEFERRED_DISCO_OBJECT_H
#define DEFERRED_DISCO_OBJECT_H

#include <memory>
#include "Drawable.h"
#include "Model.h"
#include "Transform.h"

class Object : public Drawable{
public:
    Object(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, const std::string& model, const std::string& shader, bool castsShadows = true);

    void Draw(const Camera& camera) const override;

    [[nodiscard]] Transform GetTransform() const override;
    [[nodiscard]] std::shared_ptr<Model> GetModel() const;

    Transform transform;
    bool castsShadows;
    glm::vec3 colorTint = glm::vec3(1.0f);
};


#endif //DEFERRED_DISCO_OBJECT_H
