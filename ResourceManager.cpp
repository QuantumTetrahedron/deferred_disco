

#include "ResourceManager.h"

std::map<std::string, std::shared_ptr<Model>> ResourceManager::models;
std::map<std::string, std::shared_ptr<Shader>> ResourceManager::shaders;
std::map<std::string, std::shared_ptr<Texture>> ResourceManager::textures;

void ResourceManager::LoadModel(const std::string &modelName, const std::string &modelPath) {
    if(models.find(modelName) == models.end()){
        models.emplace(modelName, std::make_shared<Model>(modelPath));
    }
}

void ResourceManager::LoadShader(const std::string &shaderName, const std::string &VSpath, const std::string &FSpath) {
    if(shaders.find(shaderName) == shaders.end()){
        shaders.emplace(shaderName, std::make_shared<Shader>(VSpath.c_str(), FSpath.c_str()));
    }
}

void ResourceManager::LoadTexture(const std::string &textureName, const std::string &texturePath) {
    if(textures.find(textureName) == textures.end()){
        textures.emplace(textureName, std::make_shared<Texture>(texturePath));
    }
}

std::shared_ptr<Model> ResourceManager::GetModel(const std::string &modelName) {
    return models.at(modelName);
}

std::shared_ptr<Shader> ResourceManager::GetShader(const std::string &shaderName) {
    return shaders.at(shaderName);
}

std::shared_ptr<Texture> ResourceManager::GetTexture(const std::string &textureName) {
    return textures.at(textureName);
}

void ResourceManager::Clean() {
    models.clear();
    shaders.clear();
    textures.clear();
}

const std::map<std::string, std::shared_ptr<Model>> &ResourceManager::GetModels() {
    return models;
}

const std::map<std::string, std::shared_ptr<Shader>> &ResourceManager::GetShaders() {
    return shaders;
}

const std::map<std::string, std::shared_ptr<Texture>> &ResourceManager::GetTextures() {
    return textures;
}
