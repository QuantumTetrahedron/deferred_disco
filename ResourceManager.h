
#ifndef DEFERRED_DISCO_RESOURCEMANAGER_H
#define DEFERRED_DISCO_RESOURCEMANAGER_H

#include <string>
#include <map>
#include <memory>
#include "Model.h"
#include "Texture.h"

class ResourceManager {
public:
    static void LoadModel(const std::string& modelName, const std::string& modelPath);
    static void LoadShader(const std::string& shaderName, const std::string& VSpath, const std::string& FSpath);
    static void LoadTexture(const std::string& textureName, const std::string& texturePath);

    static std::shared_ptr<Model> GetModel(const std::string& modelName);
    static std::shared_ptr<Shader> GetShader(const std::string& shaderName);
    static std::shared_ptr<Texture> GetTexture(const std::string& textureName);

    static void Clean();

    static const std::map<std::string, std::shared_ptr<Model>>& GetModels();
    static const std::map<std::string, std::shared_ptr<Shader>>& GetShaders();
    static const std::map<std::string, std::shared_ptr<Texture>>& GetTextures();

private:
    static std::map<std::string, std::shared_ptr<Model>> models;
    static std::map<std::string, std::shared_ptr<Shader>> shaders;
    static std::map<std::string, std::shared_ptr<Texture>> textures;
};

#endif //DEFERRED_DISCO_RESOURCEMANAGER_H
