#version 330 core

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

in vec3 FragPos;
in vec2 TexCoords;
in mat3 TBN;
in vec3 colorTint;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;

void main() {
    gPosition = FragPos;
    vec3 n = texture(texture_normal1, TexCoords).rgb;
    n = normalize(n * 2.0 - 1.0);
    gNormal = normalize(TBN * n);
    gAlbedoSpec.rgb = texture(texture_diffuse1, TexCoords).rgb * colorTint;
    gAlbedoSpec.a = texture(texture_specular1, TexCoords).r;
}
