#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in mat4 instanceMatrix;
layout(location = 8) in vec3 color;

out vec3 FragPos;
out vec2 TexCoords;
out mat3 TBN;
out vec3 colorTint;

uniform mat4 view;
uniform mat4 projection;

void main(){
    vec4 worldPos = instanceMatrix * vec4(vPosition, 1.0);
    FragPos = worldPos.xyz;
    TexCoords = vTexCoords;

    mat3 normalMatrix = transpose(inverse(mat3(instanceMatrix)));
    vec3 N = normalize(normalMatrix * vNormal);
    vec3 T = normalize(normalMatrix * vTangent);
    T = normalize(T-dot(T,N)*N);
    vec3 B = cross(N,T);
    TBN = mat3(T,B,N);

    gl_Position = projection * view * worldPos;
    colorTint = color;
}