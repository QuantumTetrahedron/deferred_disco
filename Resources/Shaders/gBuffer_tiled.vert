#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;

out vec3 FragPos;
out vec2 TexCoords;
out mat3 TBN;
out vec3 colorTint;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec2 scale;

void main(){
    vec4 worldPos = model * vec4(vPosition, 1.0);
    FragPos = worldPos.xyz;
    TexCoords = vTexCoords * scale;

    mat3 normalMatrix = transpose(inverse(mat3(model)));
    vec3 T = normalize(normalMatrix * vec3(1,0,0));
    vec3 B = normalize(normalMatrix * vec3(0,-1,0));
    vec3 N = normalize(normalMatrix * vec3(0,0,1));
    TBN = mat3(T,B,N);

    gl_Position = projection * view * worldPos;
    colorTint = vec3(1.0);
}