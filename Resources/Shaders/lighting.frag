#version 330 core

const int BATCH_SIZE = 16;

out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform sampler2D prevOut;

struct Light{
    vec3 position;
    vec3 direction;
    vec3 color;
    mat4 lightSpaceMatrix;
    sampler2D shadowMap;
};
uniform Light lights[BATCH_SIZE];

uniform vec3 viewPos;

vec3 calcLight(vec3 FragPos, vec3 Diffuse, float Specular, vec3 Normal, vec3 viewDir, float outerCutOff, float epsilon, int i){
    vec3 lightDir = normalize(lights[i].position - FragPos);
    vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * lights[i].color;
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(Normal, halfwayDir), 0.0), 16.0);
    vec3 specular = lights[i].color * spec * Specular;
    float theta = dot(lightDir, normalize(-lights[i].direction));
    float intensity = clamp((theta - outerCutOff) / epsilon, 0.0, 1.0);

    vec4 FragPosInLightSpace = lights[i].lightSpaceMatrix * vec4(FragPos, 1.0);
    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(lights[i].shadowMap, 0);
    float bias = 0.0;//max(0.05 * (1.0 - dot(Normal, lightDir)), 0.005);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(lights[i].shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    if(projCoords.z > 1.0)
        shadow = 0.0;

    return (diffuse + specular) * (1.0 - shadow) * intensity;
    //return diffuse * (1.0 - shadow) * intensity;
}

void main(){
    vec3 FragPos = texture(gPosition, TexCoords).rgb;
    vec3 Normal = texture(gNormal, TexCoords).rgb;
    vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
    float Specular = texture(gAlbedoSpec, TexCoords).a;

    vec3 lighting = texture(prevOut, TexCoords).rgb;
    //vec3 lighting = Diffuse * 0.1;
    vec3 viewDir = normalize(viewPos - FragPos);
    float cutOff = cos(radians(12.5));
    float outerCutOff = cos(radians(17.5));
    float epsilon = (cutOff - outerCutOff);
    for(int i = 0; i < BATCH_SIZE; ++i){
        lighting += calcLight(FragPos, Diffuse, Specular, Normal, viewDir, outerCutOff, epsilon, i);
    }

    FragColor = vec4(lighting, 1.0);
    //FragColor = vec4(Normal, 1.0);
}