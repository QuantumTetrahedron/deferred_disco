#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec2 scale;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;

void main() {
    FragPos = vec3(model * vec4(vPosition, 1.0));
    TexCoords = vTexCoords * scale;
    Normal = normalize(transpose(inverse(mat3(model))) * vec3(0,0,1));
    gl_Position = projection * view * model * vec4(vPosition, 1.0);
    //gl_Position = model * vec4(vPosition, 1.0);
}