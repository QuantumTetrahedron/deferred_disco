#version 330 core

out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D lightingResult;
uniform sampler2D ambientBase;

uniform float exposure;

void main(){
    vec3 Diffuse = texture(ambientBase, TexCoords).rgb;
    vec3 hdrColor = texture(lightingResult, TexCoords).rgb;

    // Add ambient
    hdrColor += Diffuse * 0.05;
    // Exposure tone mapping
    vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);
    // gamma correction
    mapped = pow(mapped, vec3(1.0 / 2.2));

    FragColor = vec4(mapped, 1.0);
    //FragColor = vec4(hdrColor, 1.0);
}