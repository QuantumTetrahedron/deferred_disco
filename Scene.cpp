
#include <sstream>
#include "Scene.h"
#include "TiledPlane.h"
#include "Object.h"
#include "SpotLight.h"


void Scene::Load(const char *filePath) {
    tinyxml2::XMLDocument doc;
    doc.LoadFile(filePath);
    auto root = doc.FirstChildElement("scene");
    if(!root) throw std::runtime_error("failed to load file");

    auto room = root->FirstChildElement("room");
    auto size = room->FirstChildElement("size");
    std::string x = size->FindAttribute("x")->Value();
    std::string y = size->FindAttribute("y")->Value();
    std::string z = size->FindAttribute("z")->Value();
    std::string sizeStr = x+" "+y+" "+z;

    std::stringstream ss(sizeStr);
    ss >> roomSize.x;
    ss >> roomSize.y;
    ss >> roomSize.z;

    LoadRoom();

    auto objs = root->FirstChildElement("objects");

    auto obj = objs->FirstChildElement("object");
    while(obj){
        std::string modelName, shaderName;
        std::stringstream posStr, rotStr, scStr;
        modelName = obj->FindAttribute("model")->Value();
        shaderName = obj->FindAttribute("shader")->Value();
        posStr = std::stringstream(obj->FindAttribute("position")->Value());
        rotStr = std::stringstream(obj->FindAttribute("rotation")->Value());
        scStr = std::stringstream(obj->FindAttribute("scale")->Value());
        glm::vec3 pos, rot, sc;
        posStr >> pos.x >> pos.y >> pos.z;
        rotStr >> rot.x >> rot.y >> rot.z;
        scStr >> sc.x >> sc.y >> sc.z;
        objects.push_back(std::make_shared<Object>(pos, rot, sc, modelName, shaderName));

        obj = obj->NextSiblingElement("object");
    }

    auto ls = root->FirstChildElement("lights");
    auto light = ls->FirstChildElement("light");
    while(light){
        std::stringstream posStr(light->FindAttribute("position")->Value());
        glm::vec3 pos;
        posStr >> pos.x >> pos.y >> pos.z;

        auto state = light->FirstChildElement("state");
        std::vector<std::pair<float, SpotLight::State>> behaviour;
        SpotLight::State s;
        float t;
        while(state){
            std::stringstream targetStr(state->FindAttribute("target")->Value());
            targetStr >> s.target.x >> s.target.y >> s.target.z;
            std::stringstream colorStr(state->FindAttribute("color")->Value());
            colorStr >> s.color.x >> s.color.y >> s.color.z;
            std::stringstream tStr(state->FindAttribute("time")->Value());
            tStr >> t;
            behaviour.emplace_back(t, s);

            state = state->NextSiblingElement("state");
        }

        float len = roomSize.y - pos.y;

        objects.push_back(std::make_shared<Object>(pos, glm::vec3(0.0f), glm::vec3(0.1f, len, 0.1f), "spotlight_shaft", "default", false));
        std::shared_ptr<Object> l = std::make_shared<Object>(pos, glm::vec3(0.0f), glm::vec3(0.1f), "spotlight_lamp", "default",false);
        objects.push_back(l);

        lights.push_back(std::make_shared<SpotLight>(pos, behaviour, l));
        light = light->NextSiblingElement("light");
    }
}

void Scene::LoadRoom() {
    walls.push_back(std::make_shared<TiledPlane>(glm::vec3(-roomSize.x / 2, roomSize.y / 2, 0.0f), glm::vec3(0.0f,90.0f,0.0f), glm::vec3(roomSize.z, roomSize.y, 1.0f), "wall", "wall_norm", "tiled"));
    walls.push_back(std::make_shared<TiledPlane>(glm::vec3( roomSize.x / 2, roomSize.y / 2, 0.0f), glm::vec3(0.0f, 270.0f,0.0f), glm::vec3(roomSize.z, roomSize.y, 1.0f), "wall","wall_norm", "tiled"));
    walls.push_back(std::make_shared<TiledPlane>(glm::vec3( 0.0f, roomSize.y / 2, roomSize.z / 2), glm::vec3(0.0f, 180.0f, 0.0f), glm::vec3(roomSize.x, roomSize.y, 1.0f), "wall","wall_norm", "tiled"));
    walls.push_back(std::make_shared<TiledPlane>(glm::vec3(0.0f, roomSize.y / 2, -roomSize.z /2), glm::vec3(0.0f, 0.0f,0.0f), glm::vec3(roomSize.x, roomSize.y, 1.0f), "wall","wall_norm", "tiled"));
    walls.push_back(std::make_shared<TiledPlane>(glm::vec3(0.0f), glm::vec3(270.0f, 0.0f,0.0f), glm::vec3(roomSize.x, roomSize.z, 1.0f), "floor", "floor_norm", "tiled"));
    walls.push_back(std::make_shared<TiledPlane>(glm::vec3(0.0f, roomSize.y, 0.0f), glm::vec3(90.0f, 0.0f,0.0f), glm::vec3(roomSize.x, roomSize.z, 1.0f), "floor", "floor_norm", "tiled"));

}
