
#ifndef DEFERRED_DISCO_SCENE_H
#define DEFERRED_DISCO_SCENE_H

#include <glm/vec3.hpp>
#include <memory>
#include "tinyxml2.h"
#include "Object.h"
#include "TiledPlane.h"
#include "SpotLight.h"

class Scene {
public:
    glm::vec3 roomSize;

    std::vector<std::shared_ptr<Object>> objects;
    std::vector<std::shared_ptr<TiledPlane>> walls;
    std::vector<std::shared_ptr<SpotLight>> lights;

    void Load(const char* filePath);
private:
    void LoadRoom();
};


#endif //DEFERRED_DISCO_SCENE_H
