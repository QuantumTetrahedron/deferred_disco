
#ifndef DEFERRED_DISCO_SHADER_H
#define DEFERRED_DISCO_SHADER_H

#include <string>
#include <glm/glm.hpp>
#include <iostream>

class Shader {
public:
    Shader(const char* vertexFilePath, const char* fragmentFilePath);

    void Use() const;
    void SetBool(const std::string& name, bool value) const;
    void SetInt(const std::string& name, int value) const;
    void SetFloat(const std::string& name, float value) const;
    void SetVec2(const std::string& name, const glm::vec2& v) const;
    void SetVec2(const std::string& name, float x, float y) const;
    void SetVec3(const std::string& name, const glm::vec3& v) const;
    void SetVec3(const std::string& name, float x, float y, float z) const;
    void SetVec4(const std::string& name, const glm::vec4& v) const;
    void SetVec4(const std::string& name, float x, float y, float z, float w) const;
    void SetMat4(const std::string& name, const glm::mat4& mat) const;

private:
    void LoadFromFile(const char* vertexFilePath, const char* fragmentFilePath);
    void checkErrors(unsigned int shader, const std::string& type);

    friend class UniformBuffer;
    unsigned int id;
};


#endif //DEFERRED_DISCO_SHADER_H
