
#include <glad/glad.h>
#include "ShadowMapper.h"
#include "ResourceManager.h"
#include "Drawable.h"
#include "Object.h"
#include "Scene.h"

void ShadowMapper::FillShadowMap(ShadowMapper::ShadowMap &shadowMap, glm::mat4 lightSpaceMatrix, const Scene& scene) {
    shadowMap.Bind();
    glClear(GL_DEPTH_BUFFER_BIT);

    glCullFace(GL_FRONT);
    auto depthShader = ResourceManager::GetShader("depth");
    depthShader->Use();
    depthShader->SetMat4("lightSpaceMatrix", lightSpaceMatrix);
    shadowMap.lightSpaceMatrix = lightSpaceMatrix;

    depthShader->Use();

    for(auto& model : ResourceManager::GetModels()){
        std::vector<std::shared_ptr<Object>> filtered;
        for(auto& o : scene.objects){
            if(o->model == model.second && o->castsShadows){
                filtered.push_back(o);
            }
        }
        std::vector<glm::mat4> transforms;
        std::transform(std::begin(filtered), std::end(filtered), std::back_inserter(transforms), [](std::shared_ptr<Object>& obj){return obj->GetTransform().GetModelMatrix();});
        std::vector<glm::vec3> colors(transforms.size(), glm::vec3(1.0f));

        if(!filtered.empty()){
            model.second->DrawInstanced(*depthShader, transforms, colors);
        }
    }
    glCullFace(GL_BACK);

    shadowMap.Unbind();
}

ShadowMapper::ShadowMap ShadowMapper::CreateNewShadowMap() {
    ShadowMap sm;
    sm.Generate();
    return sm;
}

void ShadowMapper::ShadowMap::Generate() {
    glGenFramebuffers(1, &depthMapFBO);

    const unsigned int width = 1024, height = 1024;

    glGenTextures(1, &depthMap);
    glBindTexture(GL_TEXTURE_2D, depthMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);


    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
}

void ShadowMapper::ShadowMap::UpdateShader(const Shader& shader, const std::string& name, int texNum) const {
    shader.Use();
    shader.SetMat4(name+".lightSpaceMatrix", lightSpaceMatrix);
    shader.SetInt(name+".shadowMap", 4+texNum);
    glActiveTexture(GL_TEXTURE4+texNum);
    glBindTexture(GL_TEXTURE_2D, depthMap);
}

void ShadowMapper::ShadowMap::Bind() const {
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glViewport(0,0,1024,1024);
}

void ShadowMapper::ShadowMap::Unbind() const {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0,0,1600,900);
}