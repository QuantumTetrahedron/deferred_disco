
#ifndef DEFERRED_DISCO_SHADOWMAPPER_H
#define DEFERRED_DISCO_SHADOWMAPPER_H

#include <memory>
#include "Shader.h"
#include "Drawable.h"
#include "Scene.h"

class ShadowMapper {
public:
    class ShadowMap {
    public:
        void UpdateShader(const Shader& shader, const std::string& name, int texNum) const;
    private:
        friend class ShadowMapper;
        void Bind() const;
        void Unbind() const;
        void Generate();
        glm::mat4 lightSpaceMatrix;
        unsigned int depthMapFBO, depthMap;
    };

    static void FillShadowMap(ShadowMap &shadowMap, glm::mat4 lightSpaceMatrix, const Scene& scene);
    static ShadowMap CreateNewShadowMap();
};

#endif //DEFERRED_DISCO_SHADOWMAPPER_H
