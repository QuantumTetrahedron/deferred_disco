

#include <glm/gtc/matrix_transform.hpp>
#include <utility>
#include "SpotLight.h"

void SpotLight::SetUniforms(const Shader &shader, unsigned int lightIndex) const {
    shader.Use();
    shader.SetVec3("lights["+std::to_string(lightIndex)+"].position", position);
    shader.SetVec3("lights["+std::to_string(lightIndex)+"].direction", target - position);
    shader.SetVec3("lights["+std::to_string(lightIndex)+"].color", color);
}

SpotLight::SpotLight(glm::vec3 p, glm::vec3 target, glm::vec3 c, std::shared_ptr<Object> lampObject)
: position(p), target(target), color(c), lamp(std::move(lampObject)){
    State s;
    s.target = target;
    s.color = c;
    behaviour.emplace_back(-1, s);
    stateNum = 0;
    t = 0;
    UpdateLamp();
}

glm::mat4 SpotLight::GetLightSpaceMatrix() const {
    glm::mat4 lightProjection, lightView;
    glm::mat4 lightSpaceMatrix;
    lightProjection = glm::perspective(45.0f,1.0f,0.01f,1000.0f);
    lightView = glm::lookAt(position, target, glm::vec3(0.0, 1.0, 0.0));
    lightSpaceMatrix = lightProjection * lightView;
    return lightSpaceMatrix;
}

SpotLight::SpotLight(glm::vec3 position, std::vector<std::pair<float, State>>  b, std::shared_ptr<Object> lampObject)
: behaviour(std::move(b)), position(position), lamp(std::move(lampObject)){
    stateNum = 0;
    target = behaviour[0].second.target;
    color = behaviour[0].second.color;
    t = 0;
    UpdateLamp();
}

void SpotLight::Update(float dt) {
    if(behaviour[stateNum].first == -1) return;

    t += dt;
    while(t > behaviour[stateNum].first){
        t -= behaviour[stateNum].first;
        stateNum++;
        if(stateNum >= behaviour.size()) stateNum = 0;
    }

    int thisState = stateNum;
    int nextState = thisState + 1;
    if(nextState >= behaviour.size()) nextState = 0;

    float p = t / behaviour[thisState].first;
    target = glm::mix(behaviour[thisState].second.target, behaviour[nextState].second.target, p);
    color = glm::mix(behaviour[thisState].second.color, behaviour[nextState].second.color, p);
    UpdateLamp();
}

void SpotLight::UpdateLamp() {
    glm::vec3 direction = glm::normalize(target - position);
    float d = glm::sqrt(direction.x * direction.x + direction.z * direction.z);
    float rotX = d == 0.0f ? (direction.y > 0.0f ? 90.0f : 0.0f) : glm::atan(direction.y / d);

    glm::vec3 dir2 = glm::normalize(glm::vec3(direction.x, 0.0f, direction.z));
    float cos = -dir2.z;
    glm::vec3 cross = glm::cross(glm::vec3(0.0f, 0.0f, -1.0f), dir2);
    float rotY = glm::acos(cos);
    if(cross.y < 0.0f) rotY = glm::radians(360.0f) - rotY;
    lamp->transform.rotation = glm::vec3(glm::degrees(rotX), glm::degrees(rotY), 0);

    lamp->colorTint = glm::vec3(100.0f) * color;
}
