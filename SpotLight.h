
#ifndef DEFERRED_DISCO_SPOTLIGHT_H
#define DEFERRED_DISCO_SPOTLIGHT_H

#include <vector>
#include "Object.h"

class SpotLight{
public:
    struct State{
        glm::vec3 target;
        glm::vec3 color;
    };

    SpotLight(glm::vec3 position, glm::vec3 target, glm::vec3 color, std::shared_ptr<Object> lampObject);
    SpotLight(glm::vec3 position, std::vector<std::pair<float, State>>  behaviour, std::shared_ptr<Object> lampObject);

    void Update(float dt);

    void SetUniforms(const Shader &shader, unsigned int lightIndex) const;

    [[nodiscard]] glm::mat4 GetLightSpaceMatrix() const;

    glm::vec3 position, target, color;
    float t;
    int stateNum;
    std::vector<std::pair<float, State>> behaviour;

    void UpdateLamp();
    std::shared_ptr<Object> lamp;
};


#endif //DEFERRED_DISCO_SPOTLIGHT_H
