#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h>
#include <string>

class Texture{
public:
    GLuint ID;
    GLuint width,height;
    GLuint format;

    GLuint wrapS, wrapT, filterMin, filterMax;

    Texture();
    Texture(const std::string& file);
    void Bind() const;
private:
    void Generate(unsigned int width, unsigned int height, unsigned char* data);
};

#endif
