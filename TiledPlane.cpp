
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include "TiledPlane.h"
#include "ResourceManager.h"

bool TiledPlane::initialized = false;
unsigned int TiledPlane::VAO;

TiledPlane::TiledPlane(glm::vec3 p, glm::vec3 r, glm::vec3 s, const std::string& tex, const std::string& normalTex, const std::string& sh)
:  transform(p,r,s){
    texture = ResourceManager::GetTexture(tex);
    normalMap = ResourceManager::GetTexture(normalTex);
    shader = ResourceManager::GetShader(sh);
    if(!initialized){
        Init();
    }
}

void TiledPlane::Draw(const Camera& camera) const {
    shader->Use();
    shader->SetMat4("view", camera.GetViewMatrix());
    shader->SetMat4("model", transform.GetModelMatrix());
    shader->SetVec2("scale", glm::vec2(transform.scale.x, transform.scale.y));
    shader->SetInt("texture_diffuse1", 0);
    shader->SetInt("texture_normal1", 1);

    glActiveTexture(GL_TEXTURE0);
    texture->Bind();

    glActiveTexture(GL_TEXTURE1);
    normalMap->Bind();

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void TiledPlane::Init() {
    initialized = true;
    float vertices[] = {
            // positions           // texture coords
            0.5f,  0.5f, 0.0f, 1.0f, 1.0f, // top right
            0.5f, -0.5f, 0.0f, 1.0f, 0.0f, // bottom right
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, // bottom left
            -0.5f,  0.5f, 0.0f, 0.0f, 1.0f  // top left
    };
    unsigned int indices[] = {
            0, 3, 1, // first triangle
            1, 3, 2  // second triangle
    };
    unsigned int VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
}

Transform TiledPlane::GetTransform() const {
    return transform;
}
