#ifndef DEFERRED_DISCO_TILEDPLANE_H
#define DEFERRED_DISCO_TILEDPLANE_H

#include <glm/vec3.hpp>
#include <memory>
#include "Shader.h"
#include "Texture.h"
#include "Transform.h"
#include "Drawable.h"
#include "Camera.h"

class TiledPlane : public Drawable {
public:
    TiledPlane(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, const std::string& tex, const std::string& normalTex, const std::string& sh);

    void Draw(const Camera& camera) const override;

    [[nodiscard]] Transform GetTransform() const override;
private:

    static bool initialized;
    static unsigned int VAO;
    static void Init();

    Transform transform;
    glm::vec3 color;

    std::shared_ptr<Texture> texture;
    std::shared_ptr<Texture> normalMap;
    std::shared_ptr<Shader> shader;
};

#endif //DEFERRED_DISCO_TILEDPLANE_H
