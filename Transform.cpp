

#include <glm/gtc/matrix_transform.hpp>
#include "Transform.h"

glm::mat4 Transform::GetModelMatrix() const {
    glm::mat4 m(1.0f);
    m = glm::translate(m, position);
    if(glm::length(rotation)>0.0f)
    {
        m = glm::rotate(m, glm::radians(rotation.y), glm::vec3(0.0f,1.0f,0.0f));
        m = glm::rotate(m, glm::radians(rotation.z), glm::vec3(0.0f,0.0f,1.0f));
        m = glm::rotate(m, glm::radians(rotation.x), glm::vec3(1.0f,0.0f,0.0f));
    }
    m = glm::scale(m, scale);
    return m;
}

Transform::Transform(glm::vec3 p, glm::vec3 r, glm::vec3 s)
: position(p), rotation(r), scale(s){}
