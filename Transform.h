
#ifndef DEFERRED_DISCO_TRANSFORM_H
#define DEFERRED_DISCO_TRANSFORM_H

#include <glm/glm.hpp>

class Transform {
public:
    Transform(glm::vec3 p, glm::vec3 r, glm::vec3 s);

    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;

    glm::mat4 GetModelMatrix() const;
};

#endif //DEFERRED_DISCO_TRANSFORM_H
