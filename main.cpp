#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <map>
#include "Camera.h"
#include "ResourceManager.h"
#include "DeferredRenderer.h"
#include "stb_image.h"
#include "Scene.h"

int screenWidth = 1600, screenHeight = 900;
bool keys[1024];
GLfloat lastX = screenWidth / 2, lastY = screenHeight / 2;
bool firstMouse = true;

Camera camera(glm::vec3(0.0f,1.0f,0.0f));

void processKeyboard(GLFWwindow* window, int key, int scancode, int action, int mods);
void processMouseMove(GLFWwindow* window, double xPos, double yPos);
void DoMovement();

float deltaTime = 0.0;
float lastFrame = 0.0;

int main(int argc, const char** argv) {
    if(argc != 2){
        std::cout << "Usage: ./deferred_disco path_to_scene.xml" << std::endl;
        return -1;
    }

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    auto window = glfwCreateWindow(screenWidth, screenHeight, "disco", nullptr, nullptr);
    if(!window){
        glfwTerminate();
        throw std::runtime_error("Failed to create GLFW window");
    }

    glfwMakeContextCurrent(window);

    if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)){
        throw std::runtime_error("Failed to initialize GLAD");
    }

    glfwSetKeyCallback(window, [](GLFWwindow* win, int key, int scancode, int action, int mods){
        processKeyboard(win, key, scancode, action, mods);
    });

    glfwSetCursorPosCallback(window, [](GLFWwindow* win, double xPos, double yPos){
        processMouseMove(win, xPos, yPos);
    });
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    ResourceManager::LoadShader("tiled", "Resources/Shaders/gBuffer_tiled.vert", "Resources/Shaders/gBuffer.frag");
    ResourceManager::LoadShader("default", "Resources/Shaders/gBuffer.vert", "Resources/Shaders/gBuffer.frag");
    ResourceManager::LoadShader("lightingPass", "Resources/Shaders/lighting.vert", "Resources/Shaders/lighting.frag");
    ResourceManager::LoadShader("toneMapping", "Resources/Shaders/lighting.vert", "Resources/Shaders/toneMap.frag");
    ResourceManager::LoadShader("depth", "Resources/Shaders/depth.vert", "Resources/Shaders/depth.frag");

    ResourceManager::LoadTexture("test", "Resources/Textures/test.jpg");
    ResourceManager::LoadTexture("wall", "Resources/Textures/wall.jpg");
    ResourceManager::LoadTexture("wall_norm", "Resources/Textures/wall_norm.jpg");
    ResourceManager::LoadTexture("floor", "Resources/Textures/floor_DIFF.jpg");
    ResourceManager::LoadTexture("floor_norm", "Resources/Textures/floor_NRM.png");
    ResourceManager::LoadModel("cube", "Resources/Models/Cube/Cube.obj");
    ResourceManager::LoadModel("table", "Resources/Models/Table3/Table.obj");
    ResourceManager::LoadModel("speaker", "Resources/Models/Speaker/Speaker.obj");
    ResourceManager::LoadModel("discoBall", "Resources/Models/DiscoSphere/discoSphere.obj");

    ResourceManager::LoadModel("spotlight_shaft", "Resources/Models/Spotlight/shaft.obj");
    ResourceManager::LoadModel("spotlight_lamp", "Resources/Models/Spotlight/lamp.obj");

    Scene scene;
    try {
        scene.Load(argv[1]);
    } catch (std::runtime_error& e){
        std::cout << e.what() << std::endl;
        return -1;
    }

    glm::vec3 roomSize = scene.roomSize;
    camera.SetConstraints(-roomSize.x / 2 + 0.5, roomSize.x / 2 - 0.5, -roomSize.z / 2 + 0.5, roomSize.z / 2 - 0.5);

    glm::mat4 projection = glm::perspective(45.0f, (float)screenWidth/(float)screenHeight, 0.1f, 100.0f);
    for(const auto& p : ResourceManager::GetShaders()){
        auto s = p.second;
        s->Use();
        s->SetMat4("projection", projection);
    }

    DeferredRenderer renderer;
    renderer.Init(screenWidth, screenHeight);

    int frames = 0, lastFrames = 0;
    float t;
    while (!glfwWindowShouldClose(window)) {
        auto currentFrame = (float)glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        frames++;
        t+= deltaTime;
        if(t > 1.0f){
            t -= 1.0f;
            lastFrames = frames;
            frames = 0;
            std::cout << lastFrames << std::endl;
        }

        for(auto& light : scene.lights){
            light->Update(deltaTime);
        }


        renderer.Render(camera, scene);

        glfwPollEvents();
        DoMovement();

        glfwSwapBuffers(window);
    }

    glfwTerminate();
    return 0;
}

void DoMovement()
{
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(Camera::Movement::FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(Camera::Movement::BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(Camera::Movement::LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(Camera::Movement::RIGHT, deltaTime);
}

void processKeyboard(GLFWwindow* window, int key, int scancode, int action, int mods){
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}

void processMouseMove(GLFWwindow* window, double xpos, double ypos){
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}